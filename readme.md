# Learn to Git

Ce dépôt s'appelle : "Learn to Git" ! 

> Learn to Git, ou LTG, est un dépôt d'entraînement créée par Maxime pour apprendre à utiliser Git et sûrement d'autres outils au fur et à mesure de son apprentissage technique.

Pour le formuler d'une manière plus « cool », on pourrait dire : «C'est un dépôt pour apprendre à Giter de ouf».

Roadmap :
- [x] Effectuer cet exercice de gestion des conflits et le mener à son terme.
- [x] Fusionner TLTG2 dans LTG pour n'avoir plus qu'un unique projet d'apprentissage.
- [ ] Devenir super doué en Git.
